#ifndef Pionek_h
#define Pionek_h
#include <iostream>
#include <stdio.h>

using namespace std;

class Pionek
{
    int Pionek_Bialy=0;
    int Pionek_Czerwony=1;
    int x;
    int y;
    int kolor;
    int indeks_mojego_pola;
    public:
    void stworz_bialy(int x, int y, int z);
    void stworz_czerwony(int x, int y, int z);
    int get_x ()
    {
        return x;
    }
    int get_y ()
    {
        return y;
    }
    int get_indeks()
    {
        return indeks_mojego_pola;
    }
    void set_indeks (int x)
    {
        indeks_mojego_pola=x;
    }
};
#endif // Pionek_h
