#ifndef Plansza_h
#define Plansza_h
#include <iostream>
#include <stdio.h>
#include "Pole.h"
#include "Pionek.h"

using namespace std;

class Plansza
{
    public:
    int wspolrzedna_pionka_x=0, wspolrzedna_pionka_y=0;
    Pole plansza[81];
    Pionek biale[12];
    Pionek czerwone[12];
    void stworz_plansze (int x, int y, int z, int a, int b, int p0, int p1, int p2, int p3, int p4, int p5);
    void wyswietl_plansze (int x, int y, int z);
    int wspolrzedne_na_indeks(int a, int b); //ab - wspolrzedne pola
                                             //funkcja zwroci indeks pola w tablicy pol poprzez porownywanie wspolrzednych

    void ruch_bialy();
    void ruch_czerwony();
    int wybierz_pole();
    int czy_bije(int a, int b, int c, int d);   // ab - wspolrzedne pionka, cd - pole docelowe
                                                // na podstawie roznicy odpowiednich wspolrzednych okresla czy gracz bije
                                                // nie obsluguje damek

    int czy_moge_bic(int e);        //e mowi czyj jest ruch, 0-biale 1-czerwone
                                    // jesli gracz moze bic, funkcja zwroci 1,
                                    // w przeciwnym wypadku 0

    int znajdz_indeks_bitego_pionka (int a, int b, int c, int d);   //ab-pion bijacy, cd-pole ladowania
                                                                    //funkcja zwroci indeks pionka ktory jest bity w danej kolejce
                                                                    //nie obsluguje damek

    int sprawdz_wspolrzedne(int a, int b, int c, int d, int e); //ab-pion bijacy, cd-pole ladowania e-czyj ruch
                                                                //funkcja zwroci 0 gdy wspolrzedne sa w jakis sposob niepoprawne, z informacja co jest nie tak
                                                                //jesli wszytko jest w porzadku funkcja zwraca 1

    void bij (int a, int b, int c, int d, int e);  // abcd- wspolrzedne, e - czyj ruch

    void ruch (int a, int b, int c, int d, int e);      // ab - pionek; cd- wspolrzedne docelowe; e - czyj ruch
                                                        // funkcja zmienia wlasnosci pola zaleznie jaki ruch wykona gracz
                                                        // zamienia zwykly pionek w damke jesli pole docelowe znajduje sie w 8 rzedzie (w przypadku bialych)
                                                        // lub w 1 (w przypadku czerwonych

    int czy_koniec(int e);      // funkjca sprawdza czy graczom nie skonczyly sie pionki
                                // zwroci 1 gdy ktoremus z graczy skoncza sie pionki, lub zostanie zablokowany
                                //0 gdy gracze posiadaja pionki lub moga sie ruszac

    int droga_damki (int a, int b, int c, int d, int e); //ab wspolrzedne damki, cd wspolrzedne docelowe, e czyj ruch.
                                                                // zwroci 0 gdy nie ma po drodze pionkow
                                                                // zwroci 1 gdy jest jeden pionek przeciwnika
                                                                // zwroci 2 gdy po drodze jest pionek przyjazny (blad wspolrzednych)
    void wyswietl_zasady();
    void wyswietl_instrukcje();
    int czy_moge_dalej_bic(int a, int b);   //ab - wspolrzedne pionka, e - czyj ruch (0-biale 1-czerwone).
                                            //dzia�a identycznie jak "czy_moge_bic", za wyjatkiem tego ze
                                            //sprawdza tylko 1 pole, a nie cala plansze

    void wyswietl_gre(int a, int b, int c);     //wyswietla kolejno instrukcje, plansze a przed tym czysci ekran

    int czy_ruszam_sie_do_tylu(int a, int b, int c, int d);     //ab - pionek, cd - pole docelowe
                                                                //funkcja zwraca 1 gdy pionek probuje sie ruszyc do tylu
                                                                // a 0 gdy ruch jest zgodny z zasadami

    int czy_moge_sie_ruszyc(int e);     //e wskazuje czyj jest ruch 0-biale 1-czerwone
                                        //funkcja zwraca 1 gdy mozna, 0 gdy nie ma sie ruchu
                                        //obsluguje damki

    int czy_damka_moze_bic(int e);      //e wskazuje czy jest ruch (0-biale 1-czerwone)
                                        //funkcja zwroci 1 gdy damka moze bic
                                        // 0 gdy nie ma mozliwosci bicia

    int czy_moge_sie_ruszyc_wsp(int a, int b);   //zwraca 1 gdy moge, 0 gdy nie moge
                                                //dziala tak samo jak czy moge sie ruszyc, ale dla 1 pola

    int wybierz_pionek_ruchu();        //zwroci indeks pionka do ruchu

    int gdzie_sie_ruszyc (int x);   //zwroci indeks pola docelowego
    int daj_x(int x);
    int daj_y(int x);

};
#endif // Plansza_h
