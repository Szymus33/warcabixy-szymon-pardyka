#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <stdio.h>
#include <ctime>
#include "Pole.h"
#include "Pionek.h"
#include "Plansza.h"

/*Zasady gry:

- Aby wygrac nalezy zbiæ wszystkie pionki, lub zablokowanie wszystkich pionków przeciwnika
- Piony poruszaja sie ukosnie po ciemnych polach, przesuwajac sie o 1 pole
- Piony nie moga poruszac sie do tylu
- Bicie jest obowiazkowe
- Piony moga bic zarowno w przod jak i w tyl
- Jeden pion moze bic wielokrotnie, z mozliwoscia zmiany kierunku
- Zmiana pionka w damke nastepuje po dotarciu pionka do ostatniej linii planszy, i zajmuje caly ruch
- Damki poruszaja sie o dowolna liczbe pol w jednej lini
- Damki moga bic wielokrotnie ze zmiana kierunku
- Damka nie moze zbic dwoch pionkow przeciwnika jesli stoja one w jednej lini jeden za drugim

Obsluga gry:

- Po kazdej kolejce program prosi o podanie kolejno 4 wartosci: wspolrzedna x pionka ktorym ruszamy,
                                                                wspolrzedna y pionka ktorym ruszamy
                                                                wspolrzedna x pola docelowego
                                                                wspolrzedna y pola docelowego
- Jesli program wykryje blad, lub ich nie zgodnosc wspolrzednych z zasadami gry, poprosi o ponowne ich podanie
- W przypadku bicia wielokrotnego, podajemy kolejno pola przez ktore pionek przejdzie (nie pola pionkow bitych!)
- W przypadku bicia wielokrotnego w lini damka, nalezy podac najpierw wspolrzedne pola damki, nastepnie wspolrzedne pola
  na ktorym nie ma pionka przeciwnego. Jesli damka bedzie miala mozliwosc dalszego bicia, program poprosi o wspolrzedne pola docelowego

*/
int zerowanie_zmiennych(int x)
{
    x=0;
    return x;
}

using namespace std;


int main()
{
    srand( time( NULL ) );
    int biale_pole=0;
    int czerwone_pole=1;
    int pole_z_b_pionkiem=2;
    int pole_z_c_pionkiem=3;
    int pole_indeksowe1=4;
    int pole_indeksowe2=5;
    int ilosc_pionkow_gracza=12;
    int licznik_pionkow_b=0, licznik_pionkow_c=0;
    int dlugosc_boku_planszy=9;
    int wspolrzedna_poz=0, wspolrzedna_pion=0;
    int czy_gra_sie_toczy=1;
    int zmienna_do_sprawdzania_wspolrzednych=1;
    int wspolrzedna_x_ruch, wspolrzedna_y_ruch;
    int wspolrzedna_x_pionka, wspolrzedna_y_pionka;
    int czy_bije_dalej=0;
    int pionek_dla_randoma=0;
    int pole_celu=0;
    int czy_bije=0;
    int x=0;
    int wspolrzedne_rand_x=0,wspolrzedne_rand_y=0,wspolrzedne_rand_ruch_x=0,wspolrzedne_rand_ruch_y=0;
    Plansza Pl;
    Pl.stworz_plansze(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy, licznik_pionkow_b, licznik_pionkow_c, biale_pole, czerwone_pole, pole_z_b_pionkiem, pole_z_c_pionkiem, pole_indeksowe1, pole_indeksowe2);
    zerowanie_zmiennych(wspolrzedna_pion);
    zerowanie_zmiennych(wspolrzedna_poz);
    Pl.wyswietl_zasady();
    Pl.wyswietl_instrukcje();
    Pl.wyswietl_plansze(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);
    while(czy_gra_sie_toczy)
    {
        if(Pl.czy_koniec(0))
        {
            cout<<"Koniec gry! Czerwone wygraly!"<<endl;
            czy_gra_sie_toczy=0;
        }
        else
        {
            cout<<"Ruch Bialych"<<endl;
            zmienna_do_sprawdzania_wspolrzednych=1;
            while(zmienna_do_sprawdzania_wspolrzednych)
            {
                cout<<"Podaj wspolrzedna x pionka: ";
                wspolrzedna_x_pionka=Pl.wybierz_pole();
                cout<<"Podaj wspolrzedna y pionka: ";
                wspolrzedna_y_pionka=Pl.wybierz_pole();
                cout<<"Podaj wspolrzedna x pola docelowego: ";
                wspolrzedna_x_ruch=Pl.wybierz_pole();
                cout<<"Podaj wspolrzedna y pola docelowego: ";
                wspolrzedna_y_ruch=Pl.wybierz_pole();
                if (Pl.sprawdz_wspolrzedne(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 0))
                {
                    zmienna_do_sprawdzania_wspolrzednych=0;
                }
            }
            if(Pl.czy_bije(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch)) //0 oznacza ruch bialych
            {
                Pl.bij(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 0);
                czy_bije_dalej=Pl.czy_moge_dalej_bic(wspolrzedna_x_ruch, wspolrzedna_y_ruch);
                Pl.wyswietl_gre(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);
                while (czy_bije_dalej)
                {
                    wspolrzedna_x_pionka=wspolrzedna_x_ruch;
                    wspolrzedna_y_pionka=wspolrzedna_y_ruch;

                    cout<<"kontunuuj bicie!"<<endl;
                    cout<<"Podaj wspolrzedna x pola docelowego: ";
                    wspolrzedna_x_ruch=Pl.wybierz_pole();
                    cout<<"Podaj wspolrzedna y pola docelowego: ";
                    wspolrzedna_y_ruch=Pl.wybierz_pole();

                    Pl.bij(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 0);
                    Pl.wyswietl_gre(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);
                    czy_bije_dalej=Pl.czy_moge_dalej_bic(wspolrzedna_x_ruch, wspolrzedna_y_ruch);
                }
            }
            else
            {
                Pl.ruch(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 0);
            }
            Pl.wyswietl_gre(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);
        }

        if(Pl.czy_koniec(1))
        {
            cout<<"Koniec gry! Biale wygraly!"<<endl;
            czy_gra_sie_toczy=0;
        }
        else
        {
            pionek_dla_randoma=Pl.wybierz_pionek_ruchu();
            pole_celu=Pl.gdzie_sie_ruszyc(pionek_dla_randoma);
            wspolrzedne_rand_x=Pl.daj_x(pionek_dla_randoma);
            wspolrzedne_rand_y=Pl.daj_y(pionek_dla_randoma);
            wspolrzedne_rand_ruch_x=Pl.daj_x(pole_celu);
            wspolrzedne_rand_ruch_y=Pl.daj_y(pole_celu);
            cout<<pionek_dla_randoma<<" "<<pole_celu<<" "<<czy_bije<<endl;
            cout<<wspolrzedne_rand_x<<" "<<wspolrzedne_rand_y<<endl;
            cout<<wspolrzedne_rand_ruch_x<<" "<<wspolrzedne_rand_ruch_y<<endl;

            czy_bije=Pl.czy_bije(wspolrzedne_rand_x,wspolrzedne_rand_y,wspolrzedne_rand_ruch_x,wspolrzedne_rand_ruch_y);
            if (czy_bije==1)
            {
                Pl.bij(wspolrzedne_rand_x,wspolrzedne_rand_y,wspolrzedne_rand_ruch_x,wspolrzedne_rand_ruch_y,1);
            }
            if (czy_bije==0)
            {

                Pl.ruch(wspolrzedne_rand_x,wspolrzedne_rand_y,wspolrzedne_rand_ruch_x,wspolrzedne_rand_ruch_y,1);
            }
            Pl.wyswietl_gre(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);


        }
        /*
        else
        {
            cout<<"Ruch Czerwonych"<<endl;
            zmienna_do_sprawdzania_wspolrzednych=1;
            while(zmienna_do_sprawdzania_wspolrzednych)
            {
                cout<<"Podaj wspolrzedna x pionka: ";
                wspolrzedna_x_pionka=Pl.wybierz_pole();
                cout<<"Podaj wspolrzedna y pionka: ";
                wspolrzedna_y_pionka=Pl.wybierz_pole();
                cout<<"Podaj wspolrzedna x pola docelowego: ";
                wspolrzedna_x_ruch=Pl.wybierz_pole();
                cout<<"Podaj wspolrzedna y pola docelowego: ";
                wspolrzedna_y_ruch=Pl.wybierz_pole();
                if (Pl.sprawdz_wspolrzedne(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 1))
                {
                    zmienna_do_sprawdzania_wspolrzednych=0;
                }
            }
            if(Pl.czy_bije(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch)) //1 oznacza ruch bialych
            {
                Pl.bij(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 1);
                czy_bije_dalej=Pl.czy_moge_dalej_bic(wspolrzedna_x_ruch, wspolrzedna_y_ruch);
                Pl.wyswietl_gre(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);
                while (czy_bije_dalej)
                {
                    wspolrzedna_x_pionka=wspolrzedna_x_ruch;
                    wspolrzedna_y_pionka=wspolrzedna_y_ruch;

                    cout<<"kontunuuj bicie!"<<endl;
                    cout<<"Podaj wspolrzedna x pola docelowego: ";
                    wspolrzedna_x_ruch=Pl.wybierz_pole();
                    cout<<"Podaj wspolrzedna y pola docelowego: ";
                    wspolrzedna_y_ruch=Pl.wybierz_pole();

                    Pl.bij(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 1);
                    Pl.wyswietl_gre(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);
                    czy_bije_dalej=Pl.czy_moge_dalej_bic(wspolrzedna_x_ruch, wspolrzedna_y_ruch);
                }
            }
            else
            {
                Pl.ruch(wspolrzedna_x_pionka, wspolrzedna_y_pionka, wspolrzedna_x_ruch, wspolrzedna_y_ruch, 1);
            }

            Pl.wyswietl_gre(wspolrzedna_poz, wspolrzedna_pion, dlugosc_boku_planszy);
        }*/
    }
}
