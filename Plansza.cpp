#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <conio.h>
#include <cmath>
#include <ctime>
#include "Plansza.h"
#include "Pionek.h"
#include "Pole.h"

using namespace std;

void Plansza::stworz_plansze (int x, int y, int z, int a, int b, int p0, int p1, int p2, int p3, int p4, int p5)
{
    for (int i=0; i<z*z; i++)
    {
        plansza[i].stworz_pole(x,y,i);
        if(((y%2==0) && (x%2==0)) || ((y%2!=0) && (x%2!=0)))
        {
            if((y<4) && (y>0) && (x>0)) //ustawienie bialych pionkow
            {
                biale[a].stworz_bialy(x, y, i);
                a++;
                plansza[i].set_cechy_pola(p2);
            }
            else if(y>5) //ustawienie czerwonych pionkow
            {
                czerwone[b].stworz_czerwony(x, y, i);
                b++;
                plansza[i].set_cechy_pola(p3);
            }
            else
            {
                plansza[i].set_cechy_pola(p1);
            }
        }
        else //Pola bia³e
        {
            plansza[i].set_cechy_pola(p0);
        }
        if((x==0) || (y==0))
        {
            plansza[i].set_cechy_pola(6);
        }
        x++;
        if(x==z) //Przejscie do nastepnego rzedu
        {
            x=0;
            y++;
        }
    }
}
void Plansza::wyswietl_plansze(int x, int y, int z)
{
    for (int i=0; i<81; i++)
    {
        if(x==0)
        {
            plansza[i].wyswietl_pole(y);
            //plansza[i].wyswietl_cechy();
        }
        else if(y==0)
        {
            plansza[i].wyswietl_pole(x);
            //plansza[i].wyswietl_cechy();
        }
        else
        {
            plansza[i].wyswietl_pole(x);
            //plansza[i].wyswietl_cechy();
        }
        x++;
        if(x==z)
        {
            x=0;
            y++;
            cout<<endl;
        }
    }/*
    for (int i=0; i<81; i++)
    {
        plansza[i].wyswietl_cechy();
        cout<<endl;
    }*/
}
int Plansza::wspolrzedne_na_indeks(int a, int b) //ab - wspolrzedne pola
                                                 //funkcja zwroci indeks pola w tablicy pol poprzez porownywanie wspolrzednych
{
    int x=0;
    for (int i=0; i<81; i++)
    {
        if((plansza[i].get_x()==a) && (plansza[i].get_y()==b))
        {
            x=plansza[i].get_indeks_z_planszy();
        }
    }
    return x;
}
int Plansza::wybierz_pole()
{
    int x;
    cin>>x;
    return x;
}
int Plansza::czy_moge_bic (int e)        //e mowi czyj jest ruch, 0-biale 1-czerwone, nie obsluguje damek
                                         // jesli gracz moze bic, funkcja zwroci 1,
                                         // w przeciwnym wypadku 0
{
    if (e==0)
    {
        for(int i=0; i<81; i++)
        {
            if(plansza[i].get_cechy_pola()==2)
            {
                if (((plansza[i].get_x()<3) && (plansza[i].get_x()>0) && (plansza[i].get_y()<3) && (plansza[i].get_y()>0)))
                {
                    if((plansza[i+10].get_cechy_pola()==3)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()<3) && (plansza[i].get_y()>0) && (plansza[i].get_x()<7) && (plansza[i].get_x()>2)))
                {
                    if((plansza[i+10].get_cechy_pola()==3)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i+8].get_cechy_pola()==3)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()<3) && (plansza[i].get_y()>0) && (plansza[i].get_x()>6)))
                {
                    if((plansza[i+8].get_cechy_pola()==3)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>2) && (plansza[i].get_y()<7) && (plansza[i].get_x()>0) && (plansza[i].get_y()<3)))
                {
                    if((plansza[i+10].get_cechy_pola()==3)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-8].get_cechy_pola()==3)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>2) && (plansza[i].get_y()<7) && (plansza[i].get_x()>2) && (plansza[i].get_x()<7)))
                {
                    if((plansza[i+10].get_cechy_pola()==3)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i+8].get_cechy_pola()==3)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-10].get_cechy_pola()==3)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-8].get_cechy_pola()==3)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()<7) && (plansza[i].get_y()>2) && (plansza[i].get_x()>6)))
                {
                    if((plansza[i-10].get_cechy_pola()==3)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i+8].get_cechy_pola()==3)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>6) && (plansza[i].get_x()>0) && (plansza[i].get_x()<3)))
                {
                    if((plansza[i-8].get_cechy_pola()==3)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>6) && (plansza[i].get_x()<7) && (plansza[i].get_x()>2)))
                {
                    if((plansza[i-10].get_cechy_pola()==3)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-8].get_cechy_pola()==3)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>6) && (plansza[i].get_x()>6)))
                {
                    if((plansza[i-10].get_cechy_pola()==3)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }

            }
        }
    }
    else if (e==1)
    {
        for(int i=0; i<81; i++)
        {
            if(plansza[i].get_cechy_pola()==3)
            {
                if (((plansza[i].get_x()<3) && (plansza[i].get_x()>0) && (plansza[i].get_y()<3) && (plansza[i].get_y()>0)))
                {
                    if((plansza[i+10].get_cechy_pola()==2)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()<3) && (plansza[i].get_y()>0) && (plansza[i].get_x()<7) && (plansza[i].get_x()>2)))
                {
                    if((plansza[i+10].get_cechy_pola()==2)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i+8].get_cechy_pola()==2)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()<3) && (plansza[i].get_y()>0) && (plansza[i].get_x()>6)))
                {
                    if((plansza[i+8].get_cechy_pola()==2)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>2) && (plansza[i].get_y()<7) && (plansza[i].get_x()>0) && (plansza[i].get_y()<3)))
                {
                    if((plansza[i+10].get_cechy_pola()==2)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-8].get_cechy_pola()==2)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>2) && (plansza[i].get_y()<7) && (plansza[i].get_x()>2) && (plansza[i].get_x()<7)))
                {
                    if((plansza[i+10].get_cechy_pola()==2)&&(plansza[i+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i+8].get_cechy_pola()==2)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-10].get_cechy_pola()==2)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-8].get_cechy_pola()==2)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()<7) && (plansza[i].get_y()>2) && (plansza[i].get_x()>6)))
                {
                    if((plansza[i-10].get_cechy_pola()==2)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i+8].get_cechy_pola()==2)&&(plansza[i+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>6) && (plansza[i].get_x()>0) && (plansza[i].get_x()<3)))
                {
                    if((plansza[i-8].get_cechy_pola()==2)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>6) && (plansza[i].get_x()<7) && (plansza[i].get_x()>2)))
                {
                    if((plansza[i-10].get_cechy_pola()==2)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[i-8].get_cechy_pola()==2)&&(plansza[i-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[i].get_y()>6) && (plansza[i].get_x()>6)))
                {
                    if((plansza[i-10].get_cechy_pola()==2)&&(plansza[i-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }

            }
        }
    }
    else
    {
        return 0;
    }
}
int Plansza::czy_bije(int a, int b, int c, int d) // ab - wspolrzedne pionka, cd - pole docelowe, nie obsluguje damek
                                                  // na podstawie roznicy odpowiednich wspolrzednych okresla czy gracz bije
                                                  // nie obsluguje damek. zwraca 0 gdy nie bijemy lub 1 gdy bijemy
{
    int x=0;
    x=wspolrzedne_na_indeks(a,b);
    if(( ((a-c==1) || (a-c==-1)) && ((b-d==1) || (b-d==-1)) ) && (plansza[x].get_cechy_pola()!=4) && (plansza[x].get_cechy_pola()!=5))
    {
        return 0;
    }
    else
    {
        return 1;
    }
}
int Plansza::znajdz_indeks_bitego_pionka(int a, int b, int c, int d)//ab-pion bijacy, cd-pole ladowania, obsluguje damki
                                                                    //funkcja zwroci indeks pionka ktory jest bity w danej kolejce
                                                                    //nie obsluguje damek
{
    int x=0,y=0,z=0,w=0, v=0;
    x=wspolrzedne_na_indeks(a,b);
    y=wspolrzedne_na_indeks(c,d);
    z=x-y;
    if ((abs(z)%10==0) && (z<0))
    {
        v=10;
        if ((plansza[x].get_cechy_pola()==4) || (plansza[x].get_cechy_pola()==5))
        {
            for (int i=0;i<abs(z)/10;i++)
            {
                if ((plansza[x+v].get_cechy_pola()==2) || (plansza[x+v].get_cechy_pola()==4) || (plansza[x+v].get_cechy_pola()==3) || (plansza[x+v].get_cechy_pola()==5))
                {
                    w=x+v;
                    //cout<<"_|";
                }
                else
                {
                    v=v+10;
                }
            }
        }
        else
        {
            w=x+10;
            //cout<<"_|";
        }
    }
    else if ((abs(z)%10==0) && (z>0))
    {
        v=10;
        if ((plansza[x].get_cechy_pola()==4) || (plansza[x].get_cechy_pola()==5))
        {
            for (int i=0;i<z/10;i++)
            {
                if ((plansza[x-v].get_cechy_pola()==2) || (plansza[x-v].get_cechy_pola()==4) || (plansza[x-v].get_cechy_pola()==3) || (plansza[x-v].get_cechy_pola()==5))
                {
                    w=x-v;
                    //cout<<"|-";
                }
                else
                {
                    v=v+10;
                }
            }
        }
        else
        {
            w=x-10;
            //cout<<"|-";
        }
    }
    else if ((abs(z)%8==0) && (z<0))
    {
        v=8;
        if ((plansza[x].get_cechy_pola()==4) || (plansza[x].get_cechy_pola()==5))
        {
            for (int i=0;i<abs(z)/8;i++)
            {
                if ((plansza[x+v].get_cechy_pola()==2) || (plansza[x+v].get_cechy_pola()==4) || (plansza[x+v].get_cechy_pola()==3) || (plansza[x+v].get_cechy_pola()==5))
                {
                    w=x+v;
                    //cout<<"|_";
                }
                else
                {
                    v=v+8;
                }
            }
        }
        else
        {
            w=x+8;
            //cout<<"|_";
        }
    }
    else if ((abs(z)%8==0) && (z>0))
    {
        v=8;
        if ((plansza[x].get_cechy_pola()==4) || (plansza[x].get_cechy_pola()==5))
        {
            for (int i=0;i<z/8;i++)
            {
                if ((plansza[x-v].get_cechy_pola()==2) || (plansza[x-v].get_cechy_pola()==4) || (plansza[x-v].get_cechy_pola()==3) || (plansza[x-v].get_cechy_pola()==5))
                {
                    w=x+v;
                    //cout<<"-|";
                }
                else
                {
                    v=v+8;
                }
            }
        }
        else
        {
            w=x-8;
            //cout<<"-|";
        }
    }
    return w;
}
int Plansza::sprawdz_wspolrzedne(int a, int b, int c, int d, int e) //ab-pion bijacy, cd-pole ladowania e-czyj ruch, nie obsluguje damek
                                                                    //funkcja zwroci 0 gdy wspolrzedne sa w jakis sposob niepoprawne, z informacja co jest nie tak
                                                                    //jesli wszytko jest w porzadku funkcja zwraca 1
{
    int x=0,y=0, w=0, v=0, u=0, z=0;
    y=czy_moge_bic(e);
    x=czy_bije(a,b,c,d);
    w=wspolrzedne_na_indeks(a,b);//wspolrzedne pionka
    z=czy_ruszam_sie_do_tylu(a,b,c,d);
    if (x==1)
    {
        u=znajdz_indeks_bitego_pionka(a,b,c,d);
    }
    if(z==1)
    {
        //cout<<y<<" "<<x<<" "<<e<<endl;
        cout<<"pionek nie moze ruszac sie w tyl!"<<endl;
        return 0;
    }
    v=wspolrzedne_na_indeks(c,d);//wspolrzedne docelowe
    if((y==1)&&(x==0))
    {
        //cout<<y<<" "<<x<<" "<<e<<endl;
        cout<<"Inny sposrod twoich pionkow moze bic!"<<endl;
        return 0;
    }
    else if ((plansza[u].get_cechy_pola()!=3) && (plansza[u].get_cechy_pola()!=5) && (e==0) && (x==1) && (plansza[w].get_cechy_pola()!=4))
    {
        //cout<<y<<" "<<x<<" "<<e<<" "<<u<<endl;
        cout<<"Miedzy pionkiem a polem docelowym nie stoi czerwona figura, wiec nie mozesz bic!"<<endl;
        return 0;
    }
    else if ((plansza[u].get_cechy_pola()!=2) && (plansza[u].get_cechy_pola()!=4) && (e==1) && (x==1) && (plansza[w].get_cechy_pola()!=5))
    {
        //cout<<y<<" "<<x<<" "<<e<<" "<<u<<endl;
        cout<<"Miedzy pionkiem a polem docelowym nie stoi biala figura, wiec nie mozesz bic!"<<endl;
        return 0;
    }
    else if ((plansza[v].get_cechy_pola()==3)||(plansza[v].get_cechy_pola()==2))
    {
        //cout<<y<<" "<<x<<" "<<e<<endl;
        cout<<"Na polu docelowym stoi inny pionek!!"<<endl;
        return 0;
    }
    else if(((plansza[w].get_cechy_pola()!=2) && (plansza[u].get_cechy_pola()!=4)&& (plansza[w].get_cechy_pola()!=4))&&(e==0))
    {
        //cout<<y<<" "<<x<<" "<<e<<endl;
        cout<<"Na wskazanych wspolrzednych nie stoi twoj pionek!"<<endl;
        return 0;
    }
    else if(((plansza[w].get_cechy_pola()!=3) && (plansza[u].get_cechy_pola()!=5) && (plansza[w].get_cechy_pola()!=5))&&(e==1))
    {
        //cout<<y<<" "<<x<<" "<<e<<endl;
        cout<<"Na wskazanych wspolrzednych nie stoi twoj pionek!"<<endl;
        return 0;
    }
    else if(((plansza[w].get_cechy_pola()==2) || (plansza[w].get_cechy_pola()==3)) && ((w-v!=8) && (w-v!=-8) && (w-v!=10) && (w-v!=-10)) && (x==0))
    {
        //cout<<y<<" "<<x<<" "<<e<<endl;
        cout<<"zle podales wspolrzedne!"<<endl;
        return 0;
    }
    else
    {
        //cout<<y<<" "<<x<<" "<<e<<endl;
        return 1;
    }

}
void Plansza::bij (int a, int b, int c, int d, int e) //ab-pion bijacy, cd-pole ladowania e-czyj ruch
                                                      //obsluguje damki
{
    int x=0,y=0,w=0;
    w=znajdz_indeks_bitego_pionka(a,b,c,d);
    x=wspolrzedne_na_indeks(a,b);
    y=wspolrzedne_na_indeks(c,d);
    if((e==0) && (plansza[x].get_cechy_pola()==2) && plansza[y].get_y()<8)
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(2);
        plansza[w].set_cechy_pola(1);
    }
    if((e==0) && (plansza[x].get_cechy_pola()==2) && plansza[y].get_y()==8)
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(4);
        plansza[w].set_cechy_pola(1);
    }
    if((e==0) && (plansza[x].get_cechy_pola()==4))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(4);
        plansza[w].set_cechy_pola(1);
    }
    if((e==1) && (plansza[x].get_cechy_pola()==3) && (plansza[y].get_y()>1))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(3);
        plansza[w].set_cechy_pola(1);
    }
    if((e==1) && (plansza[x].get_cechy_pola()==3) && (plansza[y].get_y()==1))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(5);
        plansza[w].set_cechy_pola(1);
    }
    if((e==1) && (plansza[x].get_cechy_pola()==5))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(5);
        plansza[w].set_cechy_pola(1);
    }
}
void Plansza::ruch (int a, int b, int c, int d, int e)  // ab - pionek; cd- wspolrzedne docelowe; e - czyj ruch, obsluguje damki
                                                        // funkcja zmienia wlasnosci pola zaleznie jaki ruch wykona gracz
                                                        // zamienia zwykly pionek w damke jesli pole docelowe znajduje sie w 8 rzedzie (w przypadku bialych)
                                                        // lub w 1 (w przypadku czerwonych
{
    int x=0,y=0;
    x=wspolrzedne_na_indeks(a,b);
    y=wspolrzedne_na_indeks(c,d);
    if((e==0) & (plansza[x].get_cechy_pola()==4))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(4);
    }
    if((e==0) && (plansza[x].get_cechy_pola()==2) && (plansza[y].get_y()==8))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(4);
    }
    if((e==0) && (plansza[x].get_cechy_pola()==2) && (plansza[y].get_y()<8))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(2);
    }
    if((e==1) && (plansza[x].get_cechy_pola()==5))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(5);
    }
    if((e==1) && (plansza[x].get_cechy_pola()==3) && (plansza[y].get_y()==1))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(5);
    }
    if((e==1) && (plansza[x].get_cechy_pola()==3) && (plansza[y].get_y()>1))
    {
        plansza[x].set_cechy_pola(1);
        plansza[y].set_cechy_pola(3);
    }
}
int Plansza::czy_koniec(int e)   // funkjca sprawdza czy graczom nie skonczyly sie pionki, obsluguje damki. e wskazuje czyj jest ruch
                                 // zwroci 1 gdy ktoremus z graczy skoncza sie pionki
                                 //0 gdy gracze posiadaja pionki
{
    int licznik_b=0, licznik_c=0;
    int czy_mam_ruch=0, czy_mam_bicie=0, czy_damka_ma_bicie=0;
    czy_mam_ruch=czy_moge_sie_ruszyc(e);
    czy_mam_bicie=czy_moge_bic(e);
    czy_damka_ma_bicie=czy_damka_moze_bic(e);
    for(int i=0; i<81; i++)
    {
            if((plansza[i].get_cechy_pola()==2) || (plansza[i].get_cechy_pola()==4))
            {
                licznik_b++;
            }
            if((plansza[i].get_cechy_pola()==3) || (plansza[i].get_cechy_pola()==5))
            {
                licznik_c++;
            }
    }
    if((licznik_b==0) || (licznik_c==0) || ((czy_moge_sie_ruszyc(e)==0) && (czy_moge_bic(e)==0) && (czy_damka_moze_bic(e)==0)))
    {
        cout<<czy_mam_ruch<<endl;
        return 1;
    }
    else
    {
        return 0;
    }
}

int Plansza::droga_damki(int a, int b, int c, int d, int e)         //ab - damka, cd - cel, e - czyj ruch, 0 biale 1 czerwone
                                                                    // zwroci 0 gdy nie ma po drodze pionkow
                                                                    // zwroci 1 gdy jest wiecej niz 1 pionek przeciwnika
                                                                    // zwroci 2 gdy po drodze jest pionek przyjazny (blad wspolrzednych)
{
    int x=0,y=0,z=0,w=0,v=0,licznik_napotkanych_pionkow_przeciwnika=0,licznik_napotkanych_pionkow_swoich=0;
    x=wspolrzedne_na_indeks(a,b); // indeks damki
    y=wspolrzedne_na_indeks(c,d); // indeks celu
    z=x-y; // znak wyniku pozwoli okreslic czy damka rusza sie w gore planszy czy w dol/ natomiast wynik dzielenia z reszta bedzie okreslal czy porusza sie w prawo czy lewo
           // ponizej zastosowana zmienna okresla o ile pol przesuwa sie damka
    if (((z%10==0) && (z<0)) || ((z%10==0) && (z>0))) //prawo w dol || lewo w gore
    {
        w=abs(z)/10;
        v=10;
        for (int i=0; i<w; i++)
        {
            if((e==0) && ((plansza[x+v].get_cechy_pola()==3) || (plansza[x+v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==1) && ((plansza[x+v].get_cechy_pola()==2) || (plansza[x+v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==0) && ((plansza[x+v].get_cechy_pola()==2) || (plansza[x+v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }
            if((e==1) && ((plansza[x+v].get_cechy_pola()==3) || (plansza[x+v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }
            if((e==0) && ((plansza[x-v].get_cechy_pola()==3) || (plansza[x-v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==1) && ((plansza[x-v].get_cechy_pola()==2) || (plansza[x-v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==0) && ((plansza[x-v].get_cechy_pola()==2) || (plansza[x+v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }
            if((e==1) && ((plansza[x-v].get_cechy_pola()==3) || (plansza[x-v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }
            v=v+10;
        }
        if ((licznik_napotkanych_pionkow_przeciwnika>1) || (licznik_napotkanych_pionkow_swoich>0))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    if (((z%8==0) && (z<0))|| ((z%8==0) && (z>0))) //lewo w dol || prawo w gore
    {
        w=abs(z)/8;
        v=8;
        for (int i=0; i<w; i++)
        {
            if((e==0) && ((plansza[x+v].get_cechy_pola()==3) || (plansza[x+v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==1) && ((plansza[x+v].get_cechy_pola()==2) || (plansza[x+v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==0) && ((plansza[x+v].get_cechy_pola()==2) || (plansza[x+v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }
            if((e==1) && ((plansza[x+v].get_cechy_pola()==3) || (plansza[x+v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }if((e==0) && ((plansza[x-v].get_cechy_pola()==3) || (plansza[x-v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==1) && ((plansza[x-v].get_cechy_pola()==2) || (plansza[x-v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_przeciwnika++;
            }
            if((e==0) && ((plansza[x-v].get_cechy_pola()==2) || (plansza[x-v].get_cechy_pola()==4)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }
            if((e==1) && ((plansza[x-v].get_cechy_pola()==3) || (plansza[x-v].get_cechy_pola()==5)))
            {
                licznik_napotkanych_pionkow_swoich++;
            }
            v=v+8;
        }
        if ((licznik_napotkanych_pionkow_przeciwnika>1) || (licznik_napotkanych_pionkow_swoich>0))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
}

void Plansza::wyswietl_zasady() // wyswietla zasady
{
    cout<<"Zasady gry: "<<endl<<endl;
    cout<<"- Aby wygrac nalezy zbiæ wszystkie pionki, lub zablokowanie wszystkich pionków przeciwnika"<<endl;
    cout<<"- Piony poruszaja sie ukosnie po ciemnych polach, przesuwajac sie o 1 pole"<<endl;
    cout<<"- Piony nie moga poruszac sie do tylu"<<endl;
    cout<<"- Bicie jest obowiazkowe"<<endl;
    cout<<"- Piony moga bic zarowno w przod jak i w tyl"<<endl;
    cout<<"- Jeden pion moze bic wielokrotnie, z mozliwoscia zmiany kierunku"<<endl;
    cout<<"- Zmiana pionka w damke nastepuje po dotarciu pionka do ostatniej linii planszy, i zajmuje caly ruch"<<endl;
    cout<<"- Damki poruszaja sie o dowolna liczbe pol w jednej lini"<<endl;
    cout<<"- Damki moga bic wielokrotnie ze zmiana kierunku"<<endl;
    cout<<"- Damka nie moze zbic dwoch pionkow przeciwnika jesli stoja one w jednej lini jeden za drugim"<<endl<<endl;
}
void Plansza::wyswietl_instrukcje() // wyswietla instrukcje
{
    cout<<"Jak grac: "<<endl<<endl;
    cout<<"- Po kazdej kolejce program prosi o podanie kolejno 4 wartosci:"<<endl;
    cout<<"wspolrzedna x pionka ktorym ruszamy"<<endl;
    cout<<"wspolrzedna y pionka ktorym ruszamy"<<endl;
    cout<<"wspolrzedna x pola docelowego"<<endl;
    cout<<"wspolrzedna y pola docelowego"<<endl;
    cout<<"- Jesli program wykryje blad, lub ich nie zgodnosc wspolrzednych z zasadami gry, poprosi o ponowne ich podanie"<<endl;
    cout<<"- W przypadku bicia wielokrotnego, podajemy kolejno pola przez ktore pionek przejdzie (nie pola pionkow bitych!), tyczy sie rowniez pojedynczego bicia"<<endl;
    cout<<"- W przypadku bicia wielokrotnego w lini damka, nalezy podac najpierw wspolrzedne pola damki, nastepnie wspolrzedne pola"<<endl;
    cout<<"  na ktorym nie ma pionka przeciwnego. Jesli damka bedzie miala mozliwosc dalszego bicia, program poprosi o wspolrzedne pola docelowego"<<endl<<endl;
}

int Plansza::czy_moge_dalej_bic (int a, int b)  //ab - wspolrzedne pionka, e - czyj ruch (0-biale 1-czerwone).
                                                //działa identycznie jak "czy_moge_bic", za wyjatkiem tego ze
                                                //sprawdza tylko 1 pole, a nie cala plansze
                                                //zwraca 1 gdy mozemy bic
{
    int x=0;
    x=wspolrzedne_na_indeks(a,b);
    if(plansza[x].get_cechy_pola()==2)
            {
                if (((plansza[x].get_x()<3) && (plansza[x].get_x()>0) && (plansza[x].get_y()<3) && (plansza[x].get_y()>0)))
                {
                    if((plansza[x+10].get_cechy_pola()==3)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()<3) && (plansza[x].get_y()>0) && (plansza[x].get_x()<7) && (plansza[x].get_x()>2)))
                {
                    if((plansza[x+10].get_cechy_pola()==3)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x+8].get_cechy_pola()==3)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()<3) && (plansza[x].get_y()>0) && (plansza[x].get_x()>6)))
                {
                    if((plansza[x+8].get_cechy_pola()==3)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>2) && (plansza[x].get_y()<7) && (plansza[x].get_x()>0) && (plansza[x].get_y()<3)))
                {
                    if((plansza[x+10].get_cechy_pola()==3)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-8].get_cechy_pola()==3)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>2) && (plansza[x].get_y()<7) && (plansza[x].get_x()>2) && (plansza[x].get_x()<7)))
                {
                    if((plansza[x+10].get_cechy_pola()==3)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x+8].get_cechy_pola()==3)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-10].get_cechy_pola()==3)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-8].get_cechy_pola()==3)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()<7) && (plansza[x].get_y()>2) && (plansza[x].get_x()>6)))
                {
                    if((plansza[x-10].get_cechy_pola()==3)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x+8].get_cechy_pola()==3)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>6) && (plansza[x].get_x()>0) && (plansza[x].get_x()<3)))
                {
                    if((plansza[x-8].get_cechy_pola()==3)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>6) && (plansza[x].get_x()<7) && (plansza[x].get_x()>2)))
                {
                    if((plansza[x-10].get_cechy_pola()==3)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-8].get_cechy_pola()==3)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>6) && (plansza[x].get_x()>6)))
                {
                    if((plansza[x-10].get_cechy_pola()==3)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }

            }
    else if(plansza[x].get_cechy_pola()==3)
            {
                if (((plansza[x].get_x()<3) && (plansza[x].get_x()>0) && (plansza[x].get_y()<3) && (plansza[x].get_y()>0)))
                {
                    if((plansza[x+10].get_cechy_pola()==2)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()<3) && (plansza[x].get_y()>0) && (plansza[x].get_x()<7) && (plansza[x].get_x()>2)))
                {
                    if((plansza[x+10].get_cechy_pola()==2)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x+8].get_cechy_pola()==2)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()<3) && (plansza[x].get_y()>0) && (plansza[x].get_x()>6)))
                {
                    if((plansza[x+8].get_cechy_pola()==2)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>2) && (plansza[x].get_y()<7) && (plansza[x].get_x()>0) && (plansza[x].get_y()<3)))
                {
                    if((plansza[x+10].get_cechy_pola()==2)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-8].get_cechy_pola()==2)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>2) && (plansza[x].get_y()<7) && (plansza[x].get_x()>2) && (plansza[x].get_x()<7)))
                {
                    if((plansza[x+10].get_cechy_pola()==2)&&(plansza[x+20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x+8].get_cechy_pola()==2)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-10].get_cechy_pola()==2)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-8].get_cechy_pola()==2)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()<7) && (plansza[x].get_y()>2) && (plansza[x].get_x()>6)))
                {
                    if((plansza[x-10].get_cechy_pola()==2)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x+8].get_cechy_pola()==2)&&(plansza[x+16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>6) && (plansza[x].get_x()>0) && (plansza[x].get_x()<3)))
                {
                    if((plansza[x-8].get_cechy_pola()==2)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>6) && (plansza[x].get_x()<7) && (plansza[x].get_x()>2)))
                {
                    if((plansza[x-10].get_cechy_pola()==2)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                    if((plansza[x-8].get_cechy_pola()==2)&&(plansza[x-16].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }
                if (((plansza[x].get_y()>6) && (plansza[x].get_x()>6)))
                {
                    if((plansza[x-10].get_cechy_pola()==2)&&(plansza[x-20].get_cechy_pola()==1))
                       {
                           return 1;
                       }
                }

            }
    else
    {
        return 0;
    }
}

void Plansza::wyswietl_gre (int a, int b, int c) //wyswietla kolejno instrukcje, plansze a przed tym czysci ekran
{
    system ("cls");
    wyswietl_instrukcje();
    wyswietl_plansze(a,b,c);
}

int Plansza::czy_ruszam_sie_do_tylu(int a, int b, int c, int d) //ab - pionek, cd - pole docelowe
                                                                //funkcja zwraca 1 gdy pionek probuje sie ruszyc do tylu
                                                                // a 0 gdy ruch jest zgodny z zasadami
{
    int x=0, y=0;
    x=wspolrzedne_na_indeks(a,b);
    y=wspolrzedne_na_indeks(c,d);
    if (plansza[x].get_cechy_pola()==2)
    {
        if(x-y<0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    if (plansza[x].get_cechy_pola()==3)
    {
        if(x-y>0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}

int Plansza::czy_moge_sie_ruszyc (int e)    //e wskazuje czyj jest ruch 0-biale 1-czerwone
                                            //funkcja zwraca 1 gdy mozna, 0 gdy nie ma sie ruchu
                                            //obsluguje damki
{
    int a=0,x=0,y=0;
        if ((e==0))
        {
            if ((plansza[10].get_cechy_pola()==2) && (plansza[20].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[28].get_cechy_pola()==2) && (plansza[38].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[46].get_cechy_pola()==2) && (plansza[56].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[64].get_cechy_pola()==2) && (plansza[74].get_cechy_pola()==1))
            {
                a=1;
            }


            if ((plansza[26].get_cechy_pola()==2) && (plansza[36].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[44].get_cechy_pola()==2) && (plansza[54].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[62].get_cechy_pola()==2) && (plansza[72].get_cechy_pola()==1))
            {
                a=1;
            }


            if ((plansza[12].get_cechy_pola()==2) && ((plansza[20].get_cechy_pola()==1) || (plansza[22].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[14].get_cechy_pola()==2) && ((plansza[22].get_cechy_pola()==1) || (plansza[24].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[16].get_cechy_pola()==2) && ((plansza[24].get_cechy_pola()==1) || (plansza[26].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[20].get_cechy_pola()==2) && ((plansza[30].get_cechy_pola()==1) || (plansza[28].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[22].get_cechy_pola()==2) && ((plansza[30].get_cechy_pola()==1) || (plansza[32].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[24].get_cechy_pola()==2) && ((plansza[32].get_cechy_pola()==1) || (plansza[34].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[30].get_cechy_pola()==2) && ((plansza[40].get_cechy_pola()==1) || (plansza[38].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[32].get_cechy_pola()==2) && ((plansza[42].get_cechy_pola()==1) || (plansza[40].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[34].get_cechy_pola()==2) && ((plansza[44].get_cechy_pola()==1) || (plansza[42].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[38].get_cechy_pola()==2) && ((plansza[48].get_cechy_pola()==1) || (plansza[46].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[40].get_cechy_pola()==2) && ((plansza[50].get_cechy_pola()==1) || (plansza[48].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[42].get_cechy_pola()==2) && ((plansza[52].get_cechy_pola()==1) || (plansza[50].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[48].get_cechy_pola()==2) && ((plansza[58].get_cechy_pola()==1) || (plansza[56].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[50].get_cechy_pola()==2) && ((plansza[60].get_cechy_pola()==1) || (plansza[58].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[52].get_cechy_pola()==2) && ((plansza[62].get_cechy_pola()==1) || (plansza[60].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[56].get_cechy_pola()==2) && ((plansza[66].get_cechy_pola()==1) || (plansza[64].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[58].get_cechy_pola()==2) && ((plansza[68].get_cechy_pola()==1) || (plansza[66].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[60].get_cechy_pola()==2) && ((plansza[70].get_cechy_pola()==1) || (plansza[68].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[66].get_cechy_pola()==2) && ((plansza[76].get_cechy_pola()==1) || (plansza[74].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[68].get_cechy_pola()==2) && ((plansza[78].get_cechy_pola()==1) || (plansza[76].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[70].get_cechy_pola()==2) && ((plansza[80].get_cechy_pola()==1) || (plansza[78].get_cechy_pola()==1)))
            {
                a=1;
            }
        }
        if ((e==1))
        {
            if ((plansza[64].get_cechy_pola()==3) && (plansza[56].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[46].get_cechy_pola()==3) && (plansza[38].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[28].get_cechy_pola()==3) && (plansza[20].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[80].get_cechy_pola()==3) && (plansza[70].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[62].get_cechy_pola()==3) && (plansza[52].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[44].get_cechy_pola()==3) && (plansza[34].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((plansza[26].get_cechy_pola()==3) && (plansza[1].get_cechy_pola()==1))
            {
                a=1;
            }

            if ((plansza[74].get_cechy_pola()==3) && ((plansza[64].get_cechy_pola()==1) || (plansza[66].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[76].get_cechy_pola()==3) && ((plansza[66].get_cechy_pola()==1) || (plansza[68].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[78].get_cechy_pola()==3) && ((plansza[68].get_cechy_pola()==1) || (plansza[70].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[70].get_cechy_pola()==3) && ((plansza[60].get_cechy_pola()==1) || (plansza[62].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[68].get_cechy_pola()==3) && ((plansza[58].get_cechy_pola()==1) || (plansza[60].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[66].get_cechy_pola()==3) && ((plansza[56].get_cechy_pola()==1) || (plansza[58].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[60].get_cechy_pola()==3) && ((plansza[50].get_cechy_pola()==1) || (plansza[52].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[58].get_cechy_pola()==3) && ((plansza[48].get_cechy_pola()==1) || (plansza[50].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[56].get_cechy_pola()==3) && ((plansza[46].get_cechy_pola()==1) || (plansza[48].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[52].get_cechy_pola()==3) && ((plansza[42].get_cechy_pola()==1) || (plansza[44].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[50].get_cechy_pola()==3) && ((plansza[40].get_cechy_pola()==1) || (plansza[42].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[48].get_cechy_pola()==3) && ((plansza[38].get_cechy_pola()==1) || (plansza[40].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[42].get_cechy_pola()==3) && ((plansza[32].get_cechy_pola()==1) || (plansza[34].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[40].get_cechy_pola()==3) && ((plansza[30].get_cechy_pola()==1) || (plansza[32].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[38].get_cechy_pola()==3) && ((plansza[30].get_cechy_pola()==1) || (plansza[28].get_cechy_pola()==1)))
            {
                a=1;
            }

            if ((plansza[34].get_cechy_pola()==3) && ((plansza[24].get_cechy_pola()==1) || (plansza[26].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[32].get_cechy_pola()==3) && ((plansza[24].get_cechy_pola()==1) || (plansza[22].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[30].get_cechy_pola()==3) && ((plansza[20].get_cechy_pola()==1) || (plansza[22].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[20].get_cechy_pola()==3) && ((plansza[10].get_cechy_pola()==1) || (plansza[12].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[22].get_cechy_pola()==3) && ((plansza[12].get_cechy_pola()==1) || (plansza[14].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((plansza[24].get_cechy_pola()==3) && ((plansza[14].get_cechy_pola()==1) || (plansza[16].get_cechy_pola()==1)))
            {
                a=1;
            }
        }
        /*
        if ((e==1))
        {
            if ((x==1) && (y>1) && (plansza[i-8].get_cechy_pola()==1) && (plansza[i].get_cechy_pola()==3))
            {
                a=1;
            }
            if ((x==8) && (y>1) && (plansza[i-10].get_cechy_pola()==1) && (plansza[i].get_cechy_pola()==3))
            {
                a=1;
            }
            if ((x>1) && (x<8) && (y>1) && ((plansza[i-8].get_cechy_pola()==1) || (plansza[i-10].get_cechy_pola()==1)) && (plansza[i].get_cechy_pola()==3))
            {
                a=1;
            }
        }*/
       for (int i=0; i<81; i=i+2)
       {
        if (((e==0) && (plansza[i].get_cechy_pola()==4)) || ((e==1) && (plansza[i].get_cechy_pola()==5)))
        {
            if ((i==10) && (plansza[20].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((i==17) && (plansza[25].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((i==73) && (plansza[65].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((i==80) && (plansza[70].get_cechy_pola()==1))
            {
                a=1;
            }
            if ((x==1) && (y<8) && (y>1) && ((plansza[i+10].get_cechy_pola()==1) || (plansza[i-8].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((x==8) && (y<8) && (y>1) && ((plansza[i-10].get_cechy_pola()==1) || (plansza[i+8].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((x>1) && (x<8) && (y==1) && ((plansza[i+10].get_cechy_pola()==1) || (plansza[i+8].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((x>1) && (x<8) && (y==8) && ((plansza[i+10].get_cechy_pola()==1) || (plansza[i-8].get_cechy_pola()==1)))
            {
                a=1;
            }
            if ((x>1) && (x<8) && (y<8) && (y>1) && ((plansza[i+10].get_cechy_pola()==1) || (plansza[i-10].get_cechy_pola()==1) || (plansza[i+8].get_cechy_pola()==1) || (plansza[i-8].get_cechy_pola()==1)))
            {
                a=1;
            }
        }
       }
        if (a==1)
        {
            return 1;
            cout<<"moge bic"<<endl;
        }
        else
        {
            return 0;
        }
    }


int Plansza::czy_damka_moze_bic(int e)      //e wskazuje czy jest ruch (0-biale 1-czerwone)
                                            //funkcja zwroci 1 gdy damka moze bic
                                            // 0 gdy nie ma mozliwosci bicia
{
    int x=0,a=0,b=0,y=0,z=0,u=0;
    int warunek=1;
    if (e==0)
    {
        for (int i=0; i<81; i++)
        {
            if (plansza[i].get_cechy_pola()==4)
            {
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();//warunki w while wskazuja w ktora strone sprawdzana jest linia bicia
                while(warunek) //lewo gora
                {
                    a=a-1;
                    b=b-1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==3) || (plansza[y].get_cechy_pola()==5))
                    {
                        z=y; //wskaze indeks pola z pionkiem przeciwnika
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y; //wskaze indeks pustego pola
                    }
                    if (u<z)
                    {
                        x=1;
                    }
                    if ((a==1) || (b==1))
                    {
                        warunek=0;
                    }
                }
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();
                while(warunek) //prawo gora
                {
                    a=a+1;
                    b=b-1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==3) || (plansza[y].get_cechy_pola()==5))
                    {
                        z=y;
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y;
                    }
                    if (u<z)
                    {
                        x=1;
                    }
                    if ((a==8) || (y==1))
                    {
                        warunek=0;
                    }
                }
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();
                while(warunek) //prawo dol
                {
                    a=a+1;
                    b=b+1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==3) || (plansza[y].get_cechy_pola()==5))
                    {
                        z=y; //wskaze indeks pola z pionkiem przeciwnika
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y; //wskaze indeks pustego pola
                    }
                    if (u>z)
                    {
                        x=1;
                    }
                    if ((a==8) || (b==8))
                    {
                        warunek=0;
                    }
                }
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();
                while(warunek) //lewo dol
                {
                    a=a-1;
                    b=b+1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==3) || (plansza[y].get_cechy_pola()==5))
                    {
                        z=y; //wskaze indeks pola z pionkiem przeciwnika
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y; //wskaze indeks pustego pola
                    }
                    if (u>z)
                    {
                        x=1;
                    }
                    if ((a==1) || (b==8))
                    {
                        warunek=0;
                    }
                }
            }
        }
    }
    if (e==1)
    {
        for (int i=0; i<81; i++)
        {
            if (plansza[i].get_cechy_pola()==5)
            {
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();//warunki w while wskazuja w ktora strone sprawdzana jest linia bicia
                while(warunek) //lewo gora
                {
                    a=a-1;
                    b=b-1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==2) || (plansza[y].get_cechy_pola()==4))
                    {
                        z=y; //wskaze indeks pola z pionkiem przeciwnika
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y; //wskaze indeks pustego pola
                    }
                    if (u<z)
                    {
                        x=1;
                    }
                    if ((a==1) || (b==1))
                    {
                        warunek=0;
                    }
                }
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();
                while(warunek) //lewo gora
                {
                    a=a+1;
                    b=b-1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==2) || (plansza[y].get_cechy_pola()==4))
                    {
                        z=y;
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y;
                    }
                    if (u<z)
                    {
                        x=1;
                    }
                    if ((a==8) || (b==1))
                    {
                        warunek=0;
                    }
                }
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();
                while(warunek) //prawo dol
                {
                    a=a+1;
                    b=b+1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==2) || (plansza[y].get_cechy_pola()==4))
                    {
                        z=y; //wskaze indeks pola z pionkiem przeciwnika
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y; //wskaze indeks pustego pola
                    }
                    if (u>z)
                    {
                        x=1;
                    }
                    if ((a==8) || (b==8))
                    {
                        warunek=0;
                    }
                }
                warunek=1;
                a=plansza[i].get_x();
                b=plansza[i].get_y();
                while(warunek) //lewo dol
                {
                    a=a-1;
                    b=b+1;
                    y=wspolrzedne_na_indeks(a,b);
                    if ((plansza[y].get_cechy_pola()==2) || (plansza[y].get_cechy_pola()==4))
                    {
                        z=y; //wskaze indeks pola z pionkiem przeciwnika
                    }
                    if (plansza[y].get_cechy_pola()==1)
                    {
                        u=y; //wskaze indeks pustego pola
                    }
                    if (u>z)
                    {
                        x=1;
                    }
                    if ((a==1) || (b==8))
                    {
                        warunek=0;
                    }
                }
            }
        }
    }
    if (x==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int Plansza::czy_moge_sie_ruszyc_wsp(int a, int b)  //funkcja dla randoma
                                                    //zwroci 1 gdy moze, 0 gdy nie moze
                                                    //sprawdza tylko czerwone pionki
{
    int x=0,y=0;
    y=wspolrzedne_na_indeks(a,b);
    if (((a=1) && (b<1)) && ((plansza[y].get_cechy_pola()==3) || (plansza[y].get_cechy_pola()==5)))
    {
        if (plansza[y-8].get_cechy_pola()==1)
        {
            x=1;
        }
    }
    else if (((a=8) && (b<1)) && ((plansza[y].get_cechy_pola()==3) || (plansza[y].get_cechy_pola()==5)))
    {
        if (plansza[y-10].get_cechy_pola()==1)
        {
            x=1;
        }
    }
    else if ((plansza[y].get_cechy_pola()==3) || (plansza[y].get_cechy_pola()==5))
    {
        if ((plansza[y-10].get_cechy_pola()==1) || (plansza[y-8].get_cechy_pola()==1))
        {
            x=1;
        }
    }
    else
    {
        x=0;
    }
    if (x==1)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int Plansza::wybierz_pionek_ruchu ()                         //zwroci indeks pionka ktorym random sie ruszy
{
    int a=0,b=0,c=0,d=0,e=0,f=0;
    int pionki_ktorymi_moge_bic[12];
    int pionki_ktorymi_moge_ruszyc[12];
    int licznik_bicia=0;
    int licznik_ruchu=0;
    int licznik_do_losowania=0;
    for (int i=0; i<81; i++)
    {
        a=plansza[i].get_x();
        b=plansza[i].get_y();
        c=czy_moge_dalej_bic(a,b);
        d=czy_moge_sie_ruszyc_wsp(a,b);
        if (c==1)
        {
            pionki_ktorymi_moge_bic[licznik_bicia]=i;
            licznik_bicia++;
        }
        if (d==1)
        {
            pionki_ktorymi_moge_ruszyc[licznik_ruchu]=i;
            licznik_ruchu++;
        }
    }
    if (c==1)
    {
        e=(rand() % licznik_bicia);
        f=pionki_ktorymi_moge_bic[e];
    }
    else
    {
        e=(rand() % licznik_ruchu);
        f=pionki_ktorymi_moge_ruszyc[e];
    }

    return f;
}

int Plansza::gdzie_sie_ruszyc (int x)   //zwroci indeks pola docelowego
{
    srand( time( NULL ) );
    int a=0,b=0,c=0,d=0,e=0,f=0,g=0;
    int dl=0,dp=0,gl=0,gp=0;
    int kierunek[4];
    int licznik_mozliwych_ruchow=0;
    int znajdz_kierunek=1;
    int prawo=0,lewo=0;
    a=plansza[x].get_x();
    b=plansza[x].get_y();
    c=czy_moge_dalej_bic(a,b);
    d=czy_moge_sie_ruszyc_wsp(a,b);
    for (int i=0; i<4; i++)
    {
        kierunek[i]=0;
    }
    if(c==1)
    {
        if((x==10) || (x==11) || (x=19) || (x=20))
        {
            e=x+20;
            dp=1;
            kierunek[1]=1;
        }
        if((x==16) || (x==17) || (x=25) || (x=26))
        {
            e=x+16;
            dl=1;
            kierunek[0]=1;
        }
        if((x==64) || (x==65) || (x=73) || (x=74))
        {
            e=x-16;
            gp=1;
            kierunek[3]=1;
        }
        if((x==70) || (x==71) || (x=79) || (x=80))
        {
            e=x-20;
            kierunek[2]=1;
            gl=1;
        }
        if(((a==1) || (a==2)) && ((b>2) && (b<7)))
        {
            if ((plansza[x-8].get_cechy_pola()==1) && (plansza[x-16].get_cechy_pola()==1))
            {
                e=x-16;
                gp=1;
                kierunek[3]=1;
                licznik_mozliwych_ruchow++;
            }
            if ((plansza[x+10].get_cechy_pola()==1) && (plansza[x+20].get_cechy_pola()==1))
            {
                e=x+20;
                dl=1;
                kierunek[0]=1;
                licznik_mozliwych_ruchow++;
            }
        }
        if(((a==7) || (a==8)) && ((b>2) && (b<7)))
        {
            if ((plansza[x+8].get_cechy_pola()==1) && (plansza[x+16].get_cechy_pola()==1))
            {
                e=x+16;
                dp=1;
                kierunek[1]=1;
                licznik_mozliwych_ruchow++;
            }
            if ((plansza[x-10].get_cechy_pola()==1) && (plansza[x-20].get_cechy_pola()==1))
            {
                e=x-20;
                gl=1;
                kierunek[2]=1;
                licznik_mozliwych_ruchow++;
            }
        }
        if(((b==7) || (b==8)) && ((a>2) && (a<7)))
        {
            if ((plansza[x-8].get_cechy_pola()==1) && (plansza[x-16].get_cechy_pola()==1))
            {
                e=x-16;
                gp=1;
                kierunek[3]=1;
                licznik_mozliwych_ruchow++;
            }
            if ((plansza[x-10].get_cechy_pola()==1) && (plansza[x-20].get_cechy_pola()==1))
            {
                e=x-20;
                gl=1;
                kierunek[2]=1;
                licznik_mozliwych_ruchow++;
            }
        }
        if(((b==1) || (b==2)) && ((a>2) && (a<7)))
        {
            if ((plansza[x+8].get_cechy_pola()==1) && (plansza[x+16].get_cechy_pola()==1))
            {
                e=x+16;
                dp=1;
                kierunek[1]=1;
                licznik_mozliwych_ruchow++;
            }
            if ((plansza[x+10].get_cechy_pola()==1) && (plansza[x+20].get_cechy_pola()==1))
            {
                e=x+20;
                dl=1;
                kierunek[0]=1;
                licznik_mozliwych_ruchow++;
            }
        }
        if ((a>2) && (a<7) && (b>2) && (b<7))
        {
            if ((plansza[x+8].get_cechy_pola()==1) && (plansza[x+16].get_cechy_pola()==1))
            {
                e=x+16;
                dp=1;
                kierunek[1]=1;
                licznik_mozliwych_ruchow++;
            }
            if ((plansza[x+10].get_cechy_pola()==1) && (plansza[x+20].get_cechy_pola()==1))
            {
                e=x+20;
                dl=1;
                kierunek[0]=1;
                licznik_mozliwych_ruchow++;
            }
            if ((plansza[x-8].get_cechy_pola()==1) && (plansza[x-16].get_cechy_pola()==1))
            {
                e=x-16;
                gp=1;
                kierunek[3]=1;
                licznik_mozliwych_ruchow++;
            }
            if ((plansza[x-10].get_cechy_pola()==1) && (plansza[x-20].get_cechy_pola()==1))
            {
                e=x-20;
                gl=1;
                kierunek[2]=1;
                licznik_mozliwych_ruchow++;
            }
        }
    }
    if ((c==1) && (licznik_mozliwych_ruchow>1))
    {
        f==(rand() % licznik_mozliwych_ruchow);
        while (znajdz_kierunek)
        {
            if(kierunek[f]!=0)
            {
                znajdz_kierunek=0;
            }
            else
            {
                if(f==3)
                {
                    f=0;
                }
                else
                {
                    f++;
                }
            }
        }
    }
    if (e!=0)
    {
        switch(f)
        {
        case 0:
            e=x+16;
            break;
        case 1:
            e=x+20;
            break;
        case 2:
            e=x-20;
            break;
        case 3:
            e=x-16;
            break;
        }
    }
    if((c==0)&&(d==1))
    {
        if ((a==1) && (b>1))
        {
            e=x-8;
        }
        if ((a==8) && (b>1))
        {
            e=x-10;
        }
        if ((a>1) && (a<8) && (b>1) && (plansza[x-10].get_cechy_pola()==1))
        {
            lewo=1;
            e=x-10;
        }
        if ((a>1) && (a<8) && (b>1) && (plansza[x-8].get_cechy_pola()==1))
        {
            prawo=1;
            e=x-8;
        }

    }
    if((c==0) && (prawo==1) && (lewo==1))
    {
        f==(rand() % 2);
        if (f==1)
        {
            e=x-10;
        }
        else
        {
            e=x-8;
        }
    }

    return e;
}

int Plansza::daj_x(int x)
{
    int a=plansza[x].get_x();
    return a;
}
int Plansza::daj_y(int x)
{
    int a=plansza[x].get_y();
    return a;
}

