#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <stdio.h>
#include "Pole.h"

using namespace std;
void Pole::stworz_pole(int x, int y, int z)
{
    set_x(x);
    set_y(y);
    set_indeks(z);
}
void Pole::wyswietl_pole(int x)
{
    HANDLE hOut;
    hOut = GetStdHandle( STD_OUTPUT_HANDLE );

    if(cechy_pola==0)
    {
        SetConsoleTextAttribute( hOut, 0x0070);
        cout<<"   ";
        //cout<<cechy_pola<<"/"<<indeks_z_planszy<<"  ";
        SetConsoleTextAttribute( hOut, 0x0007);
    }
    else if (cechy_pola==1)
    {
        SetConsoleTextAttribute( hOut, 0x0020);
        cout<<"   ";
        //cout<<cechy_pola<<"/"<<indeks_z_planszy<<"  ";
        SetConsoleTextAttribute( hOut, 0x0007);
    }
    else if(cechy_pola==2)
    {
        SetConsoleTextAttribute( hOut, 0x0027);
        cout<<" O ";
        //cout<<cechy_pola<<"/"<<indeks_z_planszy<<"  ";
        SetConsoleTextAttribute( hOut, 0x0007);
    }
    else if (cechy_pola==3)
    {
        SetConsoleTextAttribute( hOut, 0x0024);
        cout<<" O ";
        //cout<<cechy_pola<<"/"<<indeks_z_planszy<<"  ";
        SetConsoleTextAttribute( hOut, 0x0007);
    }
    else if (cechy_pola==4)
    {
        SetConsoleTextAttribute( hOut, 0x0027);
        cout<<" X ";
        //cout<<cechy_pola<<"/"<<indeks_z_planszy<<"  ";
        SetConsoleTextAttribute( hOut, 0x0007);
    }
    else if (cechy_pola==5)
    {
        SetConsoleTextAttribute( hOut, 0x0024);
        cout<<" X ";
        //cout<<cechy_pola<<"/"<<indeks_z_planszy<<"  ";
        SetConsoleTextAttribute( hOut, 0x0007);
    }
    else if (cechy_pola==6)
    {
        SetConsoleTextAttribute( hOut, 0x0047);
        cout<<" "<<x<<" ";
        //cout<<cechy_pola<<"/"<<indeks_z_planszy<<"  ";
        SetConsoleTextAttribute( hOut, 0x0007);
    }
}

