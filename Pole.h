#ifndef Pole_h
#define Pole_h
#include <iostream>
#include <stdio.h>

using namespace std;

class Pole
{

    int wspolrzedna_x, wspolrzedna_y;
    int indeks_z_planszy;
    int cechy_pola;// wartosci od 0 do 6 gdzie 0 - biale pole
                                        //     1 - puste ciemne pole
                                        //     2 - ciemne pole z bialym pionkiem
                                        //     3 - ciemne pole z czerwonym pionkiem
                                        //     4 - ciemne pole z biala dama
                                        //     5 - ciemne pole z czerwona dama
                                        //     6 - pole z indeksami
    public:
    int biale_pole=0;
    int czerwone_pole=1;
    int pole_z_b_pionkiem=2;
    int pole_z_c_pionkiem=3;
    void set_indeks(int x)
    {
        indeks_z_planszy=x;
    }
    int get_indeks_z_planszy()
    {
        return indeks_z_planszy;
    }
    void set_cechy_pola (int x)
    {
        cechy_pola=x;
    }
    int get_cechy_pola ()
    {
        return cechy_pola;
    }
    void set_x (int x)
    {
        wspolrzedna_x=x;
    }
    void set_y (int y)
    {
        wspolrzedna_y=y;
    }
    int get_x ()
    {
        return wspolrzedna_x;
    }
    int get_y ()
    {
        return wspolrzedna_y;
    }
    void stworz_pole(int x, int y, int z);
    void wyswietl_pole(int x);
    void wyswietl_cechy()
    {
        cout<<wspolrzedna_x<<" "<<wspolrzedna_y<<" "<<cechy_pola<<" "<<indeks_z_planszy<<endl;
    }
    int stan_pola (int x, int y)
    {
        if (cechy_pola==1)
        {
            return 1;
        }
        else if (cechy_pola==2)
        {
            return 2;
        }
        else if (cechy_pola==3)
        {
            return 3;
        }
        else
        {
            return 0;
        }
    }
};
#endif // Pole
